# Tilde-Run-method-in-Java




import java.util.*;

public class TildeRun {
 public static void main(String[] args){
  // consruct and fill up ArrayList
  ArrayList<String> words = new ArrayList<String>();
  words.add("four");
  words.add("score");
  words.add("and");
  words.add("seven");
  words.add("years");
  words.add("ago");
  System.out.println("words = " + words);
  
  //insert one tilde in front of each word
  for(int i = 0; i < words.size(); i += 2){
      words.add(i, "~");
    }
    System.out.println("after loop words = " + words);   
    
    //remove tildes
    for (int = 0; i < words.size(); i++){
      words.remove(i);
    }
    System.out.println("after second loop words = " + words);
   }
 }   
    
      
